<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
class ProductsControllers extends Controller
{

    public function index()
    {
        $Products = Product::all();
        return view('Backend.Products.index', compact('Products'));
    }

    public function create()
    {
        return view('Backend.Products.create');
    }

    public function store(Request $request)
    {
        try{

            $request->validate([
                'title'=> 'required|unique:categories|max:10',
                'description'=> 'min:5|max:1000|required',
            ]);


            /* $requestData=[
                           'title'=> $request->title,
                           'description'=> $request->description,
                           'is_active'=> $request->is_active == 'on'?1:0

       ];*/
            //dd($requestData);
            $requestData= $request->all();
            $requestData['is_active'] = $request->is_active == 'on'?1:0;

            $category=Product::create($requestData);

            //$request->session()->flash('status','task was successful');

            return redirect()->route('Products.index')->withstatus('Insert was successful');
            //dd($_POST);

        }catch(QueryException $exception){
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }

    }

    public function show(Product $product)
    {
        //$product = Product::findorFail($id);
        return view('Backend.Products.show', compact('product'));
    }

    public function edit(Product $product)
    {
        //$product = Product::findorFail($id);
        return view('Backend.Products.edit', compact('product'));
    }

    public function update(Request $request, Product $product)
    {

        try{
            //$product=Product::findorFail($id);

            $requestData= $request->all();
            $requestData['is_active'] = $request->is_active == 'on'?1:0;

            $product->update($requestData);

            //$request->session()->flash('status','task was successful');

            return redirect()->route('Products.index')->withstatus('Update was successful');
            //dd($_POST);

        }catch(QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }

    public function destroy(Request $request, Product $product)
    {

        try{
            // $product=Product::findorFail($id);
            $product->delete();

            return redirect()->route('Products.index')->withstatus('Delete was successful');


        }catch(QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }


}
