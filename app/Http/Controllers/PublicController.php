<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function products()
    {
        return view('frontend.products');
    }

    public function show()
    {
        return view('frontend.product_details');
    }
}
