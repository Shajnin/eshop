<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Database\QueryException;
use App\Exports\CategoriesExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class CategoryController extends Controller
{


    public function index()
    {
        $categories = Category::orderBy('created_at', 'desc')->get();
        return view('backend.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('Backend.categories.create');
    }

    public function store(Request $request)
    {
        try{


            $request->validate([
                'title'=> 'required|unique:categories|max:10',
                'description'=> 'min:5|max:1000|required',
            ]);


            /* $requestData=[
                           'title'=> $request->title,
                           'description'=> $request->description,
                           'is_active'=> $request->is_active == 'on'?1:0

       ];*/
            //dd($requestData);
            $requestData= $request->all();
            $requestData['is_active'] = $request->is_active == 'on'?1:0;

            $category=Category::create($requestData);

            //$request->session()->flash('status','task was successful');

            return redirect()->route('categories.index')->withstatus('Insert was successful');
            //dd($_POST);

        }catch(QueryException $exception){
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }

    }

    public function show(Category $category)
    {
        //$category = Category::findorFail($id);
        return view('backend.categories.show', compact('category'));
    }

    public function edit(Category $category)
    {
        //$category = Category::findorFail($id);
        return view('backend.categories.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {

        try{
            //$category=Category::findorFail($id);

            $requestData= $request->all();
            $requestData['is_active'] = $request->is_active == 'on'?1:0;

            $category->update($requestData);

            //$request->session()->flash('status','task was successful');

            return redirect()->route('categories.index')->withstatus('Update was successful');
            //dd($_POST);

        }catch(QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }


    public function destroy(Request $request, Category $category)
    {

        try{
           // $category=Category::findorFail($id);
            $category->delete();

            return redirect()->route('categories.index')->withstatus('Delete was successful');


        }catch(QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }

    public function downloadPdf()
    {
        $categories = Category::all();

        $pdf = PDF::loadView('backend.categories.pdf', compact('categories'));
        return $pdf->download('categories.pdf');

    }

    public function downloadExcel()
    {
        return Excel::download(new CategoriesExport, 'categories.xlsx');
    }

    public function trash()
    {
        $categories = Category::onlyTrashed()->orderBy('created_at', 'desc')->get();
        return view('backend.categories.trash', compact('categories'));
    }

}

