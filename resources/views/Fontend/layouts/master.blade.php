<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('ui/Fontend')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('ui/Fontend')}}/css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
@include('Fontend.layouts.partials.header')

<!-- Page Content -->
<div class="container">

  @include('Fontend.layouts.partials.Sidebar')
        <!-- /.col-lg-3 -->

@yield('content')

<!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
@include('Fontend.layouts.partials.footer')

<!-- Bootstrap core JavaScript -->
<script src="{{asset('ui/Fontend')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{asset('ui/Fontend')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
