<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/', 'PublicController@index');

Route::middleware('auth')->group(function () {


    Route::get('/home', 'HomeController@index')->name('home');


    /*Frontend*/
    Route::get('/products', 'PublicController@products');
    Route::get('/product_details', 'PublicController@show');

    Route::prefix('admin')->group(function () {
        /*Category routes*/

        Route::resource('/categories', 'CategoryController');
        Route::get('/categories/download-pdf', 'CategoryController@downloadPdf')->name('categories.download_pdf');//    Route::get('/categories', 'CategoryController@index')->name('categories.index');//list
        Route::get('/categories/download-excel', 'CategoryController@downloadExcel')->name('categories.download_excel');//    Route::get('/categories/create', 'CategoryController@create')->name('categories.create');//create
        Route::get('/categories/trash', 'CategoryController@trash')->name('categories.trash');//    Route::post('/categories', 'CategoryController@store')->name('categories.store');//store
//    Route::get('/categories/{category}', 'CategoryController@show')->name('categories.show');//show
//    Route::get('/categories/{category}/edit', 'CategoryController@edit')->name('categories.edit');//edit
//    Route::put('/categories/{category}', 'CategoryController@update')->name('categories.update');//update
//    Route::delete('/categories/{category}', 'CategoryController@destroy')->name('categories.destroy');//destroy


        /*Product routes*/
//      Route::resource('/Products', 'ProductsControllers');
        Route::get('/Products', 'ProductsControllers@index')->name('Products.index');//list
        Route::get('/Products/create', 'ProductsControllers@create')->name('Products.create');//create
        Route::post('/Products', 'ProductsControllers@store')->name('Products.store');//store
        Route::get('/Products/{product}', 'ProductsControllers@show')->name('Products.show');//show
        Route::get('/Products/{product}/edit', 'ProductsControllers@edit')->name('Products.edit');//edit
        Route::put('/Products/{product}', 'ProductsControllers@update')->name('Products.update');//update
        Route::delete('/Products/{product}', 'ProductsControllers@destroy')->name('Products.destroy');//destroy


    });


//Route::get('/home', function (){
//    return view('backend.home');
//});

});



